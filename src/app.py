def squares(x):
    y = x**2
    return y 

if __name__ == "__main__":
    print(f"The square of 10 is: {squares(10)}")